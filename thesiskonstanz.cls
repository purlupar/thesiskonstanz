%% thesiskonstanz.cls
%% Copyright 2020, 2021 David Diem
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status “maintained”.
% 
% The Current Maintainer of this work is David Diem.
%
% This work consists of the files thesiskonstanz.cls
% and example.tex.
%

% Maintainer (David Diem)'s contact: first dot last at uni-konstanz dot de


% License information for the files uniknlogo.eps and uniknlogo.pdf:
%
% Signet/Logo of the University of Konstanz:
% Source: https://www.uni-konstanz.de/universitaet/aktuelles-und-medien/online-und-print-medien-gestalten/corporate-design/logo-der-universitaet-konstanz/
% Terms of use: logo-license-German.pdf (included)

% NOTES:
%
% Compile (the tex file that uses this class) using pdflatex.



\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{thesiskonstanz}[2021/05/01 thesiskonstanz]

\LoadClass[11pt, a4paper]{report}
\RequirePackage{graphicx, afterpage}

\newcommand{\degree}{Master's Thesis}
\DeclareOption{bachelors}{\renewcommand{\degree}{Bachelor's Thesis}}

\let\@firstsupervisor\@empty
\newcommand\firstsupervisor[1]{\renewcommand\@firstsupervisor{#1}}
\let\@secondsupervisors\@empty
\newcommand\secondsupervisors[1]{\renewcommand\@secondsupervisors{#1}}
\let\@department\@empty
\newcommand\department[1]{\renewcommand\@department{#1}}
\let\@faculty\@empty
\newcommand\faculty[1]{\renewcommand\@faculty{#1}}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
\ProcessOptions\relax

\pagenumbering{gobble}

\renewcommand{\maketitle}{
 \begin{center}
 \begin{minipage}{\linewidth}
 \centering
  % Thesis title
  {\Large \MakeUppercase \@title\par}
  \vspace{2cm}
  {\Large \textbf{\degree}\par}
  % Author's name         
  \vspace{2cm}
  {\large submitted by\par}
  \vspace{0.2cm}
  {\Large \@author \par}
  \vspace{0.2cm}
  {\Large \par}
  \vspace{0.5cm}
  % University, Faculty, Department, Signet/Logo
  {\large to the\par}
  \vspace{0.2cm}
  {\Large University of Konstanz, Germany\par}
  \vspace{0.2cm}
  {\Large \@faculty\par}
  \vspace{0.2cm}                     
  {\Large \@department\par}
  \vspace{0.2cm}                     
  \includegraphics[width=9cm]{uniknlogo}\par    
  \vspace{1cm}
  % Evaluators/Supervisors
  {\Large 1\textsuperscript{st} Evaluator: \@firstsupervisor \\
  2\textsuperscript{nd} Evaluator: \@secondsupervisors \par}
  \vspace{2cm}
  % Date
  {\Large \@date}
         
  \end{minipage}
  \end{center}
 \clearpage 
 \null
 \thispagestyle{empty}
 \clearpage
}

